
import { useState } from 'react';
import Counter from './counter';


function App() {

  const [state, setState] = useState([
    { count: 0, index: 0 },
     { count: 0, index: 1 },
      { count: 0, index: 2 },
       { count: 0, index: 3 }
  ]);

  const [reload]  = useState([
    { count: 0, index: 0 },
     { count: 0, index: 1 },
      { count: 0, index: 2 },
       { count: 0, index: 3 }
  ]);
  const inc = (id) => setState(state.map(state => state.index === id ? { ...state, count: state.count + 1 } : state));
  const dec = (id) => setState(state.map(state => state.index === id ? { ...state, count: state.count>0?state.count - 1:0} : state));
  const updateState = (id)=>{
    setState(state.filter(state => state.index !== id))
  }

  function resetCurrentState(state){
    setState(state.map(x=>{return {...x, count:0}}));
  }
  console.log("current state", state)
  return (
    <div className='container'>
      <div className='cart'>{state.filter(st=>st.count!==0).length}</div>
      <div className='controls'>
          <button className='reload' onClick={()=> resetCurrentState(state)}>🔄</button>
          <button className='reload' onClick={()=>setState(reload)}>♻️</button>
      </div>
      {state.map((ele) => <Counter key={ele.index} id={ele.index} inc={inc} dec={dec} updateState={updateState}  value={ele.count}/>)}
    </div>
  )
}

export default App
